###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_SPK_CRM_Ready                                                    ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                   DIO - Cryomodule Ready Interlocks - WCC,CCC,RO                         ##  
##                                                                                          ##  
##                                                                                          ##  
############################          Version: 1.1         ###################################
# Author:  Wojciech Binczyk
# Date:    22-03-2024
# Version: v1.1 PVs added, PV names updates
##############################################################################################
# Author:  Wojciech Binczyk
# Date:    24-06-2023
# Version: v1.0


############################
#  STATUS BLOCK
############################
define_status_block()

#Relay signals
add_digital("OpMode_Auto",             ARCHIVE=True,       PV_DESC="Operation Mode Auto",       PV_ONAM="True",           PV_ZNAM="False")
add_digital("OpMode_Manual",           ARCHIVE=True,       PV_DESC="Operation Mode Manual",     PV_ONAM="True",           PV_ZNAM="False")
add_digital("Opened",                  ARCHIVE=True,       PV_DESC="Opened",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("Closed",                  ARCHIVE=True,       PV_DESC="Closed",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("Solenoid",                ARCHIVE=True,       PV_DESC="Solenoid energized",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("Relay_Reserve_01",        ARCHIVE=True,       PV_DESC="Reserve",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("Relay_Reserve_02",        ARCHIVE=True,       PV_DESC="Reserve",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("Relay_Reserve_03",        ARCHIVE=True,       PV_DESC="Reserve",                   PV_ONAM="True",           PV_ZNAM="False")

#Conditioning operation mode
add_digital("WarmCond_ON",             ARCHIVE=True,       PV_DESC="Operation Mode Warm Conditioning",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("ColdCond_ON",             ARCHIVE=True,       PV_DESC="Operation Mode Cold Conditioning",        PV_ONAM="True",           PV_ZNAM="False")
add_digital("RegularOper_ON",          ARCHIVE=True,       PV_DESC="Operation Regular",                       PV_ONAM="True",           PV_ZNAM="False")
add_digital("Cond_OFF",                ARCHIVE=True,       PV_DESC="Reserve",                                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("Condition_Reserve_01",    ARCHIVE=True,       PV_DESC="Reserve",                                 PV_ONAM="True",           PV_ZNAM="False")

#Warm Conditioning conditions
add_digital("WCC_FS_013_Flow_OK",      ARCHIVE=True,       PV_DESC="Antenna water flow OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_FS_014_Flow_OK",      ARCHIVE=True,       PV_DESC="Window water flow OK",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_FS_023_Flow_OK",      ARCHIVE=True,       PV_DESC="Antenna water flow OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_FS_024_Flow_OK",      ARCHIVE=True,       PV_DESC="Window water flow OK",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_TT_013_Temp_OK",      ARCHIVE=True,       PV_DESC="Antenna water temperature OK",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_TT_014_Temp_OK",      ARCHIVE=True,       PV_DESC="Window water temperature OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_TT_023_Temp_OK",      ARCHIVE=True,       PV_DESC="Antenna water temperature OK",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_TT_024_Temp_OK",      ARCHIVE=True,       PV_DESC="Window water temperature OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_TT_120_Temp_OK",      ARCHIVE=True,       PV_DESC="Flange temperature OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_TT_220_Temp_OK",      ARCHIVE=True,       PV_DESC="Flange temperature OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_Reserve_01_OK",       ARCHIVE=True,       PV_DESC="Reserve",                                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_Reserve_02_OK",       ARCHIVE=True,       PV_DESC="Reserve",                                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("WCC_Reserve_03_OK",       ARCHIVE=True,       PV_DESC="Reserve",                                 PV_ONAM="True",           PV_ZNAM="False")

#Cold Conditioning conditions
add_digital("CCC_FS_013_Flow_OK",      ARCHIVE=True,       PV_DESC="Antenna water flow OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_FS_014_Flow_OK",      ARCHIVE=True,       PV_DESC="Window water flow OK",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_FS_023_Flow_OK",      ARCHIVE=True,       PV_DESC="Antenna water flow OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_FS_024_Flow_OK",      ARCHIVE=True,       PV_DESC="Window water flow OK",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_TT_013_Temp_OK",      ARCHIVE=True,       PV_DESC="Antenna water temperature OK",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_TT_014_Temp_OK",      ARCHIVE=True,       PV_DESC="Window water temperature OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_TT_023_Temp_OK",      ARCHIVE=True,       PV_DESC="Antenna water temperature OK",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_TT_024_Temp_OK",      ARCHIVE=True,       PV_DESC="Window water temperature OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_TT_120_Temp_OK",      ARCHIVE=True,       PV_DESC="Flange temperature OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_TT_220_Temp_OK",      ARCHIVE=True,       PV_DESC="Flange temperature OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_TT_04_Temp_OK",       ARCHIVE=True,       PV_DESC="Cavity tank temperature",                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_TT_05_Temp_OK",       ARCHIVE=True,       PV_DESC="Cavity tank temperature",                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_TT_06_Temp_OK",       ARCHIVE=True,       PV_DESC="Cavity tank temperature",                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_TT_07_Temp_OK",       ARCHIVE=True,       PV_DESC="Cavity tank temperature",                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_PT_002_Pres_OK",      ARCHIVE=True,       PV_DESC="VLP line pressure",                       PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_PT_003_Pres_OK",      ARCHIVE=True,       PV_DESC="VLP line pressure",                       PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_LT_001_Lvl_OK",       ARCHIVE=True,       PV_DESC="Liquid level",                            PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_LT_002_Lvl_OK",       ARCHIVE=True,       PV_DESC="Liquid level",                            PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_Reserve_01_OK",       ARCHIVE=True,       PV_DESC="Reserve",                                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_Reserve_02_OK",       ARCHIVE=True,       PV_DESC="Reserve",                                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("CCC_Reserve_03_OK",       ARCHIVE=True,       PV_DESC="Reserve",                                 PV_ONAM="True",           PV_ZNAM="False")

#Regular operation conditions
add_digital("RO_FS_013_Flow_OK",       ARCHIVE=True,       PV_DESC="Antenna water flow OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_FS_014_Flow_OK",       ARCHIVE=True,       PV_DESC="Window water flow OK",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_FS_023_Flow_OK",       ARCHIVE=True,       PV_DESC="Antenna water flow OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_FS_024_Flow_OK",       ARCHIVE=True,       PV_DESC="Window water flow OK",                    PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_TT_013_Temp_OK",       ARCHIVE=True,       PV_DESC="Antenna water temperature OK",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_TT_014_Temp_OK",       ARCHIVE=True,       PV_DESC="Window water temperature OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_TT_023_Temp_OK",       ARCHIVE=True,       PV_DESC="Antenna water temperature OK",            PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_TT_024_Temp_OK",       ARCHIVE=True,       PV_DESC="Window water temperature OK",             PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_TT_120_Temp_OK",       ARCHIVE=True,       PV_DESC="Flange temperature OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_TT_220_Temp_OK",       ARCHIVE=True,       PV_DESC="Flange temperature OK",                   PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_PT_002_Pres_OK",       ARCHIVE=True,       PV_DESC="VLP line pressure",                       PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_PT_003_Pres_OK",       ARCHIVE=True,       PV_DESC="VLP line pressure",                       PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_LT_001_Lvl_OK",        ARCHIVE=True,       PV_DESC="Liquid level",                            PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_LT_002_Lvl_OK",        ARCHIVE=True,       PV_DESC="Liquid level",                            PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_CV_04_FB_Pres_OK",       PV_DESC="CV 04 feedback pressure OK",              PV_ONAM="True",        PV_ZNAM="False")
add_digital("RO_Reserve_01_OK",        ARCHIVE=True,       PV_DESC="Reserve",                                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_Reserve_02_OK",        ARCHIVE=True,       PV_DESC="Reserve",                                 PV_ONAM="True",           PV_ZNAM="False")
add_digital("RO_Reserve_03_OK",        ARCHIVE=True,       PV_DESC="Reserve",                                 PV_ONAM="True",           PV_ZNAM="False")

#Alarm signals
add_major_alarm("Alarm","Alarm",           PV_ZNAM="NominalState")
add_minor_alarm("Warning","Warning",       PV_ZNAM="NominalState")

#Feedback
#Warm Conditioning Setpoint
add_analog("WCC_Twater-RB",          "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="WCC water temperature setpoint",                 PV_EGU="C")
add_analog("WCC_Tfpc-RB",            "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="WCC flange temperature setpoint",                PV_EGU="K")
add_analog("WCC_Reserve_01-RB",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("WCC_Reserve_02-RB",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("WCC_Reserve_03-RB",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")

#Cold Conditioning Setpoint
add_analog("CCC_Twater-RB",         "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="CCC water temperature setpoint",                 PV_EGU="C")
add_analog("CCC_Tfpc-RB",           "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="CCC flange temperature setpoint",                PV_EGU="K")
add_analog("CCC_Tcold-RB",          "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="CCC cavity tank temperature setpoint",           PV_EGU="K")
add_analog("CCC_Pvlp-RB",           "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="CCC VLP line pressure setpoint",                 PV_EGU="mbar")
add_analog("CCC_Lthr-RB",           "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="CCC Liquid level setpoint",                      PV_EGU="%")
add_analog("CCC_Reserve_01-RB",     "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("CCC_Reserve_02-RB",     "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("CCC_Reserve_03-RB",     "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")

#Regular Operation
add_analog("RO_Twater-RB",           "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="RO water temperature setpoint",                  PV_EGU="C")
add_analog("RO_Tfpc-RB",             "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="RO flange temperature setpoint",                 PV_EGU="K")
add_analog("RO_Pvlp-RB",             "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="RO VLP line pressure setpoint",                  PV_EGU="mbar")
add_analog("RO_Lthr-RB",             "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="RO Liquid level setpoint",                       PV_EGU="%")
add_analog("RO_Reserve_01-RB",       "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("RO_Reserve_02-RB",       "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("RO_Reserve_03-RB",       "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")

############################
#  COMMAND BLOCK
############################ 
define_command_block()

#Relay commands
add_digital("Cmd_Auto",                PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              PV_DESC="CMD: Manual Mode")
add_digital("Cmd_ManuOpen",            PV_DESC="CMD: Manual Open")
add_digital("Cmd_ManuClose",           PV_DESC="CMD: Manual Close")

#Conditioning commands
add_digital("Cmd_WarmCond",            PV_DESC="CMD: Change OpMode to Warm Conditioning")
add_digital("Cmd_ColdCond",            PV_DESC="CMD: Change OpMode to Cold Conditioning")
add_digital("Cmd_RegularOper",         PV_DESC="CMD: Change OpMode to Regular Operation")
add_digital("Cmd_StopCond",            PV_DESC="CMD: Change OpMode to Conditioning OFF ")

#Reserve
add_digital("Cmd_Reserve_01",           PV_DESC="Reserve")
add_digital("Cmd_Reserve_02",           PV_DESC="Reserve")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Warm Conditioning Setpoint
add_analog("WCC_Twater-SP",          "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="WCC water temperature setpoint",                 PV_EGU="C")
add_analog("WCC_Tfpc-SP",            "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="WCC flange temperature setpoint",                PV_EGU="K")
add_analog("WCC_Reserve_01-SP",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("WCC_Reserve_02-SP",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("WCC_Reserve_03-SP",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")

#Cold Conditioning Setpoint
add_analog("CCC_Twater-SP",          "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="CCC water temperature setpoint",                 PV_EGU="C")
add_analog("CCC_Tfpc-SP",            "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="CCC flange temperature setpoint",                PV_EGU="K")
add_analog("CCC_Tcold-SP",           "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="CCC cavity tank temperature setpoint",           PV_EGU="K")
add_analog("CCC_Pvlp-SP",            "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="CCC VLP line pressure setpoint",                 PV_EGU="mbar")
add_analog("CCC_Lthr-SP",            "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="CCC Liquid level setpoint",                      PV_EGU="%")
add_analog("CCC_Reserve_01-SP",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("CCC_Reserve_02-SP",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("CCC_Reserve_03-SP",      "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")

#Regular Operation
add_analog("RO_Twater-SP",           "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="RO water temperature setpoint",                  PV_EGU="C")
add_analog("RO_Tfpc-SP",             "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="RO flange temperature setpoint",                 PV_EGU="K")
add_analog("RO_Pvlp-SP",             "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="RO VLP line pressure setpoint",                  PV_EGU="mbar")
add_analog("RO_Lthr-SP",             "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="RO Liquid level setpoint",                       PV_EGU="%")
add_analog("RO_Reserve_01-SP",       "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("RO_Reserve_02-SP",       "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")
add_analog("RO_Reserve_03-SP",       "REAL", PV_PREC="2", ARCHIVE=" 1Hz",              PV_DESC="Reserve",                                        PV_EGU="K")

#Signal Name and Description
add_string("SignalName", 30, PV_VAL="[PLCF#SignalName0]", PV_PINI="YES")
add_string("SignalDesc", 30, PV_VAL="[PLCF#SignalDescription0]", PV_PINI="YES")
